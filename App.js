import { StatusBar } from "expo-status-bar";
import React from "react";
import { StyleSheet } from "react-native";
import HomeScreen from "./src/HomeScreen";
import UserNameInputScreen from "./src/UserNameInputScreen";
import UserProfileScreen from "./src/UserProfileScreen";
import { NavigationContainer } from "@react-navigation/native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createStackNavigator } from "@react-navigation/stack";
export default function App() {
  const Tab = createBottomTabNavigator();
  const ScreenStack = createStackNavigator();
  const StackScreen = () => {
    return (
      <ScreenStack.Navigator>
        <ScreenStack.Screen name="Eingabe" component={UserNameInputScreen} />
        <ScreenStack.Screen name="Anzeige" component={UserProfileScreen} />
      </ScreenStack.Navigator>
    );
  };

  return (
    <NavigationContainer>
      <Tab.Navigator
        tabBarOptions={{
          activeTintColor: "#4196F6",
          inactiveTintColor: "gray"
        }}
      >
        <Tab.Screen
          name="Eingabe"
          component={StackScreen}
          options={{ title: "Eingabe" }}
        />
        <Tab.Screen
          name="Hello"
          component={HomeScreen}
          options={{ title: "Screen B" }}
        />
      </Tab.Navigator>
    </NavigationContainer>
  );
}
