import React from "react";
import { View, SafeAreaView } from "react-native";
import { Text } from "react-native-elements";

const UserProfileScreen = ({ route, navigation }) => {
  const { userName } = route.params;

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <Text h1>{userName}</Text>
      </View>
    </SafeAreaView>
  );
};
export default UserProfileScreen;
