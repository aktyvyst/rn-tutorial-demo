import React from 'react';
import { View, SafeAreaView } from 'react-native';
import { Text } from 'react-native-elements';

const HomeScreen = () => {
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text h1>Hello World</Text>
      </View>
    </SafeAreaView>
  )
}

export default HomeScreen;
