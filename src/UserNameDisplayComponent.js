import React from 'react';
import { View } from 'react-native';
import { Text } from 'react-native-elements';

const UserNameDisplayComponent = (props) => {
  const { userName } = props;
  return (
    <View style={{
      width: '100%',
      minWidth: '100%',
      height: '20%',
      minHeight: '20%',
      backgroundColor: 'gray',
      marginLeft: 20,
      marginRight: 20,
      marginTop: 80,
      borderRadius: 30,
      justifyContent: 'center',
      alignItems: 'center'
    }}>
      <Text h1 style={{ color: 'white' }} >{userName}</Text>
    </View>
  )
}
export default UserNameDisplayComponent;