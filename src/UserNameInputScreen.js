import React, { useState } from "react";
import { SafeAreaView, View } from "react-native";
import { Input, Text, Button } from "react-native-elements";
import UserNameDisplayComponent from "./UserNameDisplayComponent";

const UserNameInputScreen = (props) => {
  const { navigation } = props;
  const [userName, setUserName] = useState("");
  const [currentText, setCurrentText] = useState("");
  return (
    <SafeAreaView style={styles.safeArea}>
      <View style={styles.container}>
        <Input
          label="Mein Benutzername"
          value={currentText}
          onChangeText={(text) => {
            setCurrentText(text);
          }}
        />
        <Button
          disabled={currentText.length === 0}
          title="Anzeigen"
          onPress={() => {
            setUserName(currentText);
            navigation.navigate("Anzeige", {
              userName: currentText
            });
          }}
        />
        <UserNameDisplayComponent userName={userName} />
      </View>
    </SafeAreaView>
  );
};

const styles = {
  safeArea: {
    flex: 1
  },
  container: {
    flex: 1,
    // backgroundColor: '#fff',
    alignItems: "center",
    justifyContent: "center",
    padding: 25
  }
};

export default UserNameInputScreen;
